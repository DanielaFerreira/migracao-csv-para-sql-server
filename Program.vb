Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileIO
Imports System.IO
Imports System.Text


Module Program

    Function AbreConexaoDB(strConnection As String) As SqlConnection
        Dim myConn As SqlConnection
        myConn = New SqlConnection(strConnection)
        Try
            myConn.Open()
            Console.WriteLine("Conectou com o banco de dados.")
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return myConn
    End Function

    Sub SalvaEstadoNoBD(myConn As SqlConnection, caminhoDoArquivo As String)
        Dim aff As Integer
        Dim estado As String
        Dim index As Integer
        Dim linha As String
        Dim strArr() As String
        Dim myCmd As SqlCommand
        Dim StreamEncoding As Encoding
        Dim file As FileStream

        StreamEncoding = Encoding.UTF8
        myCmd = myConn.CreateCommand

        file = New FileStream(caminhoDoArquivo, FileMode.Open, FileAccess.Read)
        Using leitor As StreamReader = New StreamReader(file, Encoding.GetEncoding("iso-8859-1"))
            leitor.ReadLine()
            index = 1

            linha = leitor.ReadLine()
            While linha IsNot Nothing
                strArr = linha.Split(",")
                estado = Replace(strArr(17), """", "")
                If String.Compare(estado, "RIO DE JANEIRO") = 0 Then
                    estado = "RJ"
                ElseIf String.Compare(estado, "RIO GRANDE DO SUL") = 0 Then
                    estado = "RS"
                End If
                'Console.WriteLine(estado)
                If Not String.IsNullOrEmpty(estado) Then
                    myCmd.CommandText = $"IF NOT EXISTS (SELECT UFID FROM Estados WHERE Estado = '{estado}') BEGIN INSERT INTO Estados (UFID, Estado) VALUES ({index},'{estado}') END"
                    aff = myCmd.ExecuteNonQuery()
                    If aff > 0 Then
                        index += 1
                    End If
                End If
                linha = leitor.ReadLine()
            End While
            leitor.Close()
        End Using

    End Sub

    Sub SalvaCidadeNoBD(myConn As SqlConnection, caminhoDoArquivo As String)
        Dim aff As Integer
        Dim cidade As String
        Dim index As Integer
        Dim linha As String
        Dim strArr() As String
        Dim myCmd As SqlCommand
        Dim StreamEncoding As Encoding
        Dim file As FileStream

        StreamEncoding = Encoding.UTF8
        myCmd = myConn.CreateCommand

        file = New FileStream(caminhoDoArquivo, FileMode.Open, FileAccess.Read)
        Using leitor As StreamReader = New StreamReader(file, Encoding.GetEncoding("iso-8859-1"))
            leitor.ReadLine()
            index = 1

            linha = leitor.ReadLine()
            While linha IsNot Nothing
                strArr = linha.Split(",")
                cidade = Replace(strArr(7), """", "")
                'Console.WriteLine(cidade)
                If Not String.IsNullOrEmpty(cidade) Then
                    myCmd.CommandText = $"IF NOT EXISTS (SELECT * FROM Cidades WHERE Nome = '{cidade}') BEGIN INSERT INTO Cidades (CidadeID, Nome) VALUES ({index},'{cidade}') END"
                    aff = myCmd.ExecuteNonQuery()
                    If aff > 0 Then
                        index += 1
                    End If
                End If
                linha = leitor.ReadLine()
            End While
            file.Close()
            leitor.Close()
        End Using

    End Sub

    Sub SalvaBairroNoBD(myConn As SqlConnection, caminhoDoArquivo As String)
        Dim aff As Integer
        Dim bairro As String
        Dim cidade As String
        Dim index As Integer
        Dim linha As String
        Dim strArr() As String
        Dim myCmd As SqlCommand
        Dim StreamEncoding As Encoding
        Dim file As FileStream

        StreamEncoding = Encoding.UTF8
        myCmd = myConn.CreateCommand

        file = New FileStream(caminhoDoArquivo, FileMode.Open, FileAccess.Read)
        Using leitor As StreamReader = New StreamReader(file, Encoding.GetEncoding("iso-8859-1"))
            leitor.ReadLine()
            index = 1

            linha = leitor.ReadLine()
            While linha IsNot Nothing
                strArr = linha.Split(",")
                bairro = Replace(strArr(6), """", "")
                cidade = Replace(strArr(7), """", "")
                'Console.WriteLine(bairro)
                If Not String.IsNullOrEmpty(bairro) Then
                    myCmd.CommandText = $"IF NOT EXISTS (SELECT * FROM Bairros WHERE Nome = '{bairro}') BEGIN INSERT INTO Bairros (BairroID, CidadeID, Nome) VALUES ({index}, (SELECT CidadeID FROM Cidades WHERE Nome = '{cidade}'),'{bairro}') END"
                    aff = myCmd.ExecuteNonQuery()
                    If aff > 0 Then
                        index += 1
                    End If
                End If
                linha = leitor.ReadLine()
            End While
            file.Close()
            leitor.Close()
        End Using

    End Sub

    Sub SalvaClienteNoBD(myConn As SqlConnection, caminhoDoArquivo As String)
        Dim aff As Integer
        Dim cliente As String
        Dim index As Integer
        Dim sit As String
        Dim bairro As String
        Dim cidade As String
        Dim estado As String
        Dim data_nasc As String
        Dim sexo As String
        Dim cpf As String
        Dim rg As String
        Dim fone1 As String
        Dim fone2 As String
        Dim logradouro As String
        Dim cep As String
        Dim numero As String
        Dim data_cad As String
        Dim tipo_p As Integer
        Dim linha As String
        Dim strArr() As String
        Dim myCmd As SqlCommand
        Dim StreamEncoding As Encoding
        Dim file As FileStream
        Dim query As String

        StreamEncoding = Encoding.UTF8
        myCmd = myConn.CreateCommand

        file = New FileStream(caminhoDoArquivo, FileMode.Open, FileAccess.Read)
        Using leitor As StreamReader = New StreamReader(file, Encoding.GetEncoding("iso-8859-1"))
            leitor.ReadLine()
            index = 1

            linha = leitor.ReadLine()
            While linha IsNot Nothing
                strArr = linha.Split(",")
                sit = Replace(strArr(1), """", "")
                cliente = Replace(strArr(2), """", "")
                cpf = Replace(strArr(3), """", "")
                rg = Replace(strArr(4), """", "")
                fone1 = Replace(strArr(5), """", "")
                bairro = Replace(strArr(6), """", "")
                cidade = Replace(strArr(7), """", "")
                fone2 = Replace(strArr(8), """", "")
                cep = Replace(strArr(10), """", "")
                numero = Replace(strArr(12), """", "")
                logradouro = $"{Replace(strArr(11), """", "")}, {numero}"
                tipo_p = Replace(strArr(13), """", "")
                data_nasc = Replace(strArr(14), """", "")
                data_cad = Replace(strArr(15), """", "")
                sexo = Replace(strArr(16), """", "")
                estado = Replace(strArr(17), """", "")

                If String.Compare(sexo, "Feminino") = 0 Then
                    sexo = "F"
                ElseIf String.Compare(sexo, "Masculino") = 0 Then
                    sexo = "M"
                End If

                If String.Compare(estado, "RIO DE JANEIRO") = 0 Then
                    estado = "RJ"
                ElseIf String.Compare(estado, "RIO GRANDE DO SUL") = 0 Then
                    estado = "RS"
                End If

                If Not String.IsNullOrEmpty(cliente) Then
                    query = $"IF NOT EXISTS (SELECT * FROM Clientes WHERE Nome = '{cliente}') BEGIN INSERT INTO Clientes (ClienteID, Nome, Situacao, BairroID, CidadeID, UFID, DataNascimento, Sexo, CPF, RG, Telefone, Celular, Endereco, CEP, DataCadastro, TipoCliente) VALUES ({index}, '{cliente}', '{sit}', (SELECT BairroID FROM Bairros WHERE Nome = '{bairro}'), (SELECT CidadeID FROM Cidades WHERE Nome = '{cidade}'), (SELECT UFID FROM Estados WHERE Estado = '{estado}'), '{data_nasc}', '{sexo}', '{cpf}', '{rg}', '{fone1}', '{fone2}', '{logradouro}', '{cep}', '{data_cad}', {tipo_p}) END"
                    'Console.WriteLine(query)
                    myCmd.CommandText = query
                    aff = myCmd.ExecuteNonQuery()
                    'Console.WriteLine(aff)
                    If aff > 0 Then
                        index += 1
                    End If
                End If
                linha = leitor.ReadLine()
            End While
            file.Close()
            leitor.Close()
        End Using

    End Sub

    Sub SalvaEmailNoBD(myConn As SqlConnection, caminhoDoArquivo As String)
        Dim query As String
        Dim aff As Integer
        Dim cliente As String
        Dim email As String
        Dim emailPattern1 As String()
        Dim emailPattern2 As String()
        Dim index As Integer
        Dim linha As String
        Dim strArr() As String
        Dim myCmd As SqlCommand
        Dim StreamEncoding As Encoding
        Dim file As FileStream

        StreamEncoding = Encoding.UTF8
        myCmd = myConn.CreateCommand

        file = New FileStream(caminhoDoArquivo, FileMode.Open, FileAccess.Read)
        Using leitor As StreamReader = New StreamReader(file, Encoding.GetEncoding("iso-8859-1"))
            leitor.ReadLine()
            index = 1

            linha = leitor.ReadLine()
            While linha IsNot Nothing
                strArr = linha.Split(",")
                cliente = Replace(strArr(2), """", "")
                email = Replace(strArr(9), """", "")

                If Not String.IsNullOrEmpty(email) Then

                    emailPattern1 = email.Split("..")
                    Console.WriteLine(emailPattern1.Length)
                    If emailPattern1.Length > 1 Then
                        email = emailPattern1(1)
                    End If

                    emailPattern2 = email.Split("__")
                    If emailPattern2.Length > 1 Then
                        email = emailPattern2(1)
                    End If

                    query = $"IF NOT EXISTS (SELECT * FROM EmailsClientes WHERE Email = '{email}') BEGIN INSERT INTO EmailsClientes (ClienteID, Email, Padrao) VALUES ((SELECT ClienteID FROM Clientes WHERE Nome = '{cliente}'), '{email}', 1) END"
                    'Console.WriteLine(query)
                    myCmd.CommandText = query
                    aff = myCmd.ExecuteNonQuery()
                    If aff > 0 Then
                        index += 1
                    End If
                End If
                linha = leitor.ReadLine()
            End While
            file.Close()
            leitor.Close()
        End Using

    End Sub

    Sub Main(args As String())
        Dim strConnection As String = "Initial Catalog=Cadastros;Data Source=DESKTOP-RH99SHU\SQLEXPRESS;Integrated Security=SSPI;"
        Dim caminhoDoArquivo As String = "C:\Avaliacao\CSV\PESSOAS.csv"

        Dim myConn As SqlConnection = AbreConexaoDB(strConnection)
        SalvaEstadoNoBD(myConn, caminhoDoArquivo)
        SalvaCidadeNoBD(myConn, caminhoDoArquivo)
        SalvaBairroNoBD(myConn, caminhoDoArquivo)
        SalvaClienteNoBD(myConn, caminhoDoArquivo)
        SalvaEmailNoBD(myConn, caminhoDoArquivo)
    End Sub
End Module
